
// Takes an action to invoke when a condition is met.
function doWhen(cond, action) {
    if(truthy(cond))
        return action();
    else
        return undefined;
}

// Takes message as a string and will throw an Error with that message.
function fail(message) {
    throw new Error(message);
}

// Takes a warning as a string, and will log the warning to the console.
function warn(warning) {
    console.log(["WARNING:", warning].join(' '));
}

// Takes a note as a string, and will log that note to the console.
function note(note) {
    console.log(["NOTE:", note].join(' '));
}

// Tells you whether a value is indicative of a truthy value
function truthy(value) {
    return (value !== false) && existy(value);
}

// Tells you whether a value is non-null and not undefined
function existy(value) {
    return value != null;
}

// Returns the nth item in an indexable type, indicated type index.
function nth(indexableType, index) {
    if (!_.isNumber(index)) fail("Expected a number as the index");
    if (!isIndexed(indexableType)) fail("Not supported on non-indexed type");
    if ((index < 0) || (index > indexableType.length - 1))
        fail("Index value is out of bounds");

    return indexableType[index];
}

// Returns the second item in an indexable type
function second(indexableType) {
    return nth(indexableType, 1);
}

// Tells you whether a dataType is indexable
function isIndexed(data) {
    return _.isArray(data) || _.isString(data);
}

function cat() {
    var head = _.first(arguments);
    if (existy(head))
        return head.concat.apply(head, _.rest(arguments));
    else
        return [];
}

function construct(head, tail) {
    return cat([head], _.toArray(tail));
}

function log(msg) {
    $("<p>").text(msg).appendTo(document.body);
}

function _repeatedly(times, fun) {
    return _.map(_.range(times), fun);
}