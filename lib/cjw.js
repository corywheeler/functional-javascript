function existy(x) { return x != null };

function truthy(x) { return (x !== false) && existy(x) };

function nth(a, index) {
    if (!_.isNumber(index)) fail("Expected a number as the index");
    if (!isIndexed(a)) fail("Not supported on non-indexed type");
    if ((index < 0) || (index > a.length - 1))
        fail("Index value is out of bounds");

    return a[index];
}

function second(a) {
    return nth(a, 1);
}

function isIndexed(data) {
    return _.isArray(data) || _.isString(data);
}

function fail(thing) {
    throw new Error(thing);
}